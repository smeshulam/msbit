import React, { Component } from 'react';
import axios from 'axios';

import Product from './components/Product';
import ProductListTop from './components/ProductsListTop';
import ProductsList from './components/ProductsList';
import Pager from './components/Pager';
import Popup from './components/Popup';

import './App.css';

class App extends Component {
  state = {
    allProducts: [],
    selectedProductId: 1,
    currentPageIndex: 0,
    sortBy: 'name',
    searchQuery: '',
    numOfPages: 0,
    showPopUp: false
  };

  //Items per page
  ITEMS_PER_PAGE = 5;
  //Api url
  JSON_URL = 'https://msbit-exam-products-store.firebaseio.com/deliveryProducts/products.json';
  //Collator for sorting alphabet & numeric
  COLLATOR = new Intl.Collator('en', { numeric: true, sensitivity: 'base' });

  componentDidMount() {
    //Get json data from api
    axios.get(this.JSON_URL)
      .then(res => {
        //Set state of all products (flattened)
        this.setState({
          allProducts: res.data.map(data => this.flatten(data))
        }, () => {
          //setState Callback does nothing - just for learning
        });
      });
  }

  // Flaten object to one level
  flatten = function (data) {
    var result = {};
    function recurse(cur, prop) {
      prop = prop.replace(/^.+\./, '');
      if (Object(cur) !== cur) {
        result[prop] = cur;
      } else if (Array.isArray(cur)) {
        for (var i = 0, l = cur.length; i < l; i++)
          recurse(cur[i], prop + "[" + i + "]");
        if (l === 0)
          result[prop] = [];
      } else {
        var isEmpty = true;
        for (var p in cur) {
          isEmpty = false;
          recurse(cur[p], prop ? prop + "." + p : p);
        }
        if (isEmpty && prop)
          result[prop] = {};
      }
    }
    recurse(data, "");
    return result;
  }

  //Handle compare for sort function
  compare = (a, b) => {
    const sortBy = this.state.sortBy;
    if (sortBy === 'name') {
      //Sort by name
      return this.COLLATOR.compare(a[sortBy], b[sortBy])
    } else if (sortBy === 'price') {
      //Sort by date
      return a[sortBy] - b[sortBy];
    }
  }

  //Get slice of 5 sorted items - return slice of sorted items
  getSliceItems = () => {
    const { currentPageIndex } = this.state;
    // 0,5 | 5,10 ... // set from & to for slice
    const from = currentPageIndex * this.ITEMS_PER_PAGE;
    const to = (currentPageIndex + 1) * this.ITEMS_PER_PAGE;
    //Retrund sorted products by soryBy value and slice to a 5 item slice (from, to)
    return this.searchProduct().sort(this.compare).slice(from, to)
  }

  //Get product by id - return single product
  getProductById = (id) => {
    return this.state.allProducts.find(product => product.id === this.state.selectedProductId)
  }

  //Filter products by query - return array of filtered products
  searchProduct = () => {
    const { searchQuery } = this.state
    //Duplicate product list so we dont mutate original state
    let searchedProducts = [...this.state.allProducts]
    if (searchQuery.replace(/\s/g, "") !== '') {
      //If searchQuery is available - filter products by name OR description
      searchedProducts = searchedProducts.filter(product => product.name.includes(searchQuery) ||
        product.description.includes(searchQuery));
    }
    return searchedProducts
  }

  //Handle sort by - on select change
  handleSortBy = (e) => {
    this.setState({ sortBy: e.target.value })
  }

  //Handle query change by search input
  onQueryChange = (e) => {
    this.setState({ searchQuery: e.target.value, currentPageIndex: 0 })
  }

  //Handle product save on product form submit
  onProductSave = (product) => {
    const { allProducts } = this.state;
    //Get product index for changeing the product values in the new object
    const productIndex = allProducts.findIndex(function (p) {
      return p.id == product.id;
    });

    //Copy allProducts from state to avoid mutation
    var productsCopy = allProducts.slice();
    //Update the products copied object @ productIndex with new values from form submit
    productsCopy[productIndex].name = product.name;
    productsCopy[productIndex].description = product.description;
    productsCopy[productIndex].price = product.price;
    //Replce updated copy of products with allproducts in state
    this.setState({ allProducts: productsCopy });
    this.togglePopup();
  }

  //Handles item on click - set selected product id in state
  itemOnClick = (e) => {
    this.setState({ selectedProductId: parseInt(e.currentTarget.id) });
  }

  //Handle onclick of prev and next button of pager
  onPagerClick = (e) => {
    const value = parseInt(e.currentTarget.getAttribute('t-id'));
    this.setState((state) => ({ currentPageIndex: state.currentPageIndex + value }))
  }

  togglePopup = () => {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  render() {
    const { selectedProductId, currentPageIndex } = this.state;
    const selectedProduct = this.getProductById(selectedProductId);
    const currentProducts = this.searchProduct();

    return (
      <React.Fragment>
        <div className="wrapper">
          <header>
            <h1>My Store</h1>
          </header>
          <div className="container">
            <div className="left-side">
              <ProductListTop onQueryChange={this.onQueryChange} handleSortBy={this.handleSortBy} />
              <ProductsList getSliceItems={this.getSliceItems} itemOnClick={this.itemOnClick} />
              {
                currentProducts.length !== 0 ?
                  <Pager
                    currentPageIndex={currentPageIndex}
                    currentProducts={currentProducts}
                    itemPerPage={this.ITEMS_PER_PAGE}
                    onPagerClick={this.onPagerClick} />
                  : null
              }
            </div>
            <div className="right-side">
              {
                selectedProduct && selectedProduct.id ?
                  <div key={selectedProduct.id} className="product-form-wrapper">
                    <Product
                      id={selectedProduct.id}
                      thumbnailUrl={selectedProduct.thumbnailUrl}
                      name={selectedProduct.name}
                      description={selectedProduct.description}
                      price={selectedProduct.price}
                      onProductSave={this.onProductSave} />
                  </div>
                  : null
              }
            </div>
          </div>
        </div>
        {
          this.state.showPopup ?
            <Popup
              productName={selectedProduct.name}
              closePopup={this.togglePopup.bind(this)}
            />
            : null
        }
      </React.Fragment>
    )
  };
}

export default App;
