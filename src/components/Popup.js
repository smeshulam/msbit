import React from 'react';

export default function Popup(props) {
    return (
        <div className='popup'>
            <div className='popup_inner'>
                <h1>Thank you for updating {props.productName}</h1>
                <button onClick={props.closePopup}>X</button>
            </div>
        </div>
    );
}