import React, { Component } from 'react';

class Product extends Component {
  state = {
    isValidated: true
  }

  onSubmit = (e) => {
    e.preventDefault();
    const formFieldsArray = Array.from(e.target.elements);
    const fields = [];
    formFieldsArray.forEach(field => {
      fields[field.name] = field.value
    })
    this.props.onProductSave(fields);
  }

  validate = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value
    let valid = true;

    if ((targetName === 'name' || targetName === 'description')) {
      if (targetValue.replace(/\s/g, "") === '') {
        valid = false;
      }
    }
    if (targetName === 'price' && targetValue < 1) {
      valid = false;
    }
    this.setState({ isValidated: valid });
  }

  render() {
    return (
      <div className="product-form" id="form">
        <form onSubmit={this.onSubmit}>
          <fieldset>
            <legend>
              {this.props.name} Details
            </legend>
            <ul>
              <div className="form-row">
                <li>
                  <img onLoad={(e) => { window.VanillaTilt.init(e.currentTarget); }} src={this.props.thumbnailUrl} />
                </li>
              </div>
              <li>
                <input
                  type="hidden"
                  name="id"
                  defaultValue={this.props.id}
                />
              </li>
              <li>
                <div className="form-row">
                  <div>
                    <label htmlFor="name">
                      Name
                  </label>
                  </div>
                  <div>
                    <input
                      type="text"
                      name="name"
                      placeholder="Product Name"
                      defaultValue={this.props.name}
                      onChange={this.validate}
                    />
                  </div>
                </div>
              </li>
              <li>
                <div className="form-row">
                  <div>
                    <label htmlFor="description">
                      Description
                  </label>
                  </div>
                  <div>
                    <textarea name="description" defaultValue={this.props.description} onChange={this.validate} />
                  </div>
                </div>
              </li>
              <li>
                <div className="form-row">
                  <div>
                    <label htmlFor="description">
                      Price
                  </label>
                  </div>
                  <div>
                    <input
                      type="number"
                      name="price"
                      placeholder="Product Price"
                      defaultValue={this.props.price}
                      onChange={this.validate}
                    />
                  </div>
                </div>
              </li>
              <li>
                <div className="form-row">
                  <input
                    type="submit"
                    value="Save"
                    className="save"
                    disabled={!this.state.isValidated}
                  />
                </div>
              </li>
            </ul>
          </fieldset>
        </form>
      </div>
    )
  };
}

export default Product;
