import React from 'react';

export default function Pager(props) {
  const sliceItems = props.getSliceItems();

  if (sliceItems.length > 0) {
    return (
      //If sliceItems contain items return the items
      <ul className="products-list">
        {
          sliceItems.map(item => (
            <li key={item.id} id={item.id} onClick={props.itemOnClick}>
              <img onLoad={(e) => { window.VanillaTilt.init(e.currentTarget); }} src={item.thumbnailUrl} />
              <div className="product-info">
                <h2>{item.name}</h2>
                <span>{item.description}</span>
              </div>
              <button className="del">Delete</button>
            </li>
          ))
        }
      </ul>
    );
  } else {
    //Else return "No data to display"
    return (
      <div>No data to display</div>
    );
  }
}
