import React from 'react';

export default function Pager(props) {
    const {currentPageIndex, currentProducts, itemPerPage} = props;
    const currentPage = currentProducts.length === 0 ? currentPageIndex : currentPageIndex + 1;
    const pagerLength = Math.ceil(currentProducts.length / itemPerPage);
    
    return (
        <div className="pager">
            <button 
                disabled={currentPageIndex <= 0} 
                t-id={-1} 
                onClick={props.onPagerClick}>
                        &lt;-Prev
            </button>
            <div className="pager-numering">
                {currentPage} / {pagerLength}
            </div>
            <button 
                disabled={currentPageIndex >= pagerLength -1} t-id={1}
                onClick={props.onPagerClick}>
                        Next-&gt;
            </button>
        </div>
    );
  }
